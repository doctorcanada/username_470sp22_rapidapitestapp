package edu.uscb.csci470;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import org.json.JSONObject;

import java.net.URLEncoder;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
/*
import com.google.gson.JsonObject; // unused import (this code uses org.json.JSONObject, imported above)
*/

/**
 * This is an example of a possible Java servlet that makes a call to get data 
 * from one of the APIs available at RapidAPI.com. This was derived from a 
 * combination of one of the example servlet/JSP-based apps in Murach's 
 * "Java Servlets and JSP" 3rd edition along with selected source code from
 * the RapidAPI "How to use an API with Java" tutorial, available at:
 * https://rapidapi.com/blog/how-to-use-an-api-with-java/
 * 
 * Also: for this example, you'll need to be subscribed to the OpenWeatherMap
 * API at RapidAPI (https://rapidapi.com/community/api/open-weather-map)
 * 
 * @author username@email.uscb.edu
 * @version ICE for 21 March 2022
 */
public class RapidAPITestServlet extends HttpServlet {

	/**
	 * This method specifies what will happen when the submitted form sends 
	 * data to this servlet via a POST request
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String message = "default message"; // this is the string that will eventually be forwarded 
		
		request.setAttribute("message", message);
		
		// forward the request to the new (or in this case, reloaded) JSP page
		String url = "/index.jsp";
		getServletContext()
	        .getRequestDispatcher(url)
	        .forward(request, response);
		
	} // end method doPost

} // end class RapidAPITestServlet
